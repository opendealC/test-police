import 'intersection-observer'

import ImageObjectFit from "@utils/image-object-fit"
import AnimationScroll from "@utils/animation-scroll"
import ConstantHeight from "@utils/constant-height"
import Modale from "@utils/modale"

// Vue.component('ExampleComponent', () => import('./Components/ExampleComponent.vue'))

// Fonts
import "typeface-roboto"
import "typeface-merriweather"

// Stimulus
import "@controllers"

document.addEventListener('DOMContentLoaded', () => {
  new ImageObjectFit()
  new AnimationScroll()
  new ConstantHeight()
  new Modale()
})
