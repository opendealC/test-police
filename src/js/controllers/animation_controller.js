import { Controller } from 'stimulus'
import { gsap, TimelineLite, CSSPlugin } from "gsap/all"

gsap.registerPlugin(CSSPlugin)

export default class extends Controller {
  static targets = ["title", "separator", "description", "button", "image", "imageBackground"]

  connect () {
    this.initializeStyleBeforeAnimation()
  }

  initializeStyleBeforeAnimation () {
    const tl = new TimelineLite({ onComplete: this.addIntersectionObserver() })

    if (this.hasTitleTarget) {
      tl.to(this.titleTarget, { opacity: 0, y: -20, duration: 0 })
    }

    if (this.hasSeparatorTarget) {
      tl.to(this.separatorTarget, { width: "0", opacity: 0, y: -20, duration: 0 })
    }

    if (this.hasDescriptionTarget) {
      tl.to(this.descriptionTarget, { opacity: 0, y: -20, duration: 0 })
    }

    if (this.hasButtonTarget) {
      tl.to(this.buttonTarget, { opacity: 0, y: -20, duration: 0 })
    }

    if (this.hasImageTarget) {
      tl.to(this.imageTarget, { opacity: 0, y: -20, duration: 0 })
    }

    if (this.hasImageBackgroundTarget) {
      tl.to(this.imageBackgroundTarget, { height: "100%", duration: 0 })
    }
  }

  addIntersectionObserver () {
    const optionsObservateur = {
      rootMargin: '0px',
      threshold: 0.3
    }

    const observateur = new IntersectionObserver(this.intersectingHandler.bind(this), optionsObservateur)
    observateur.observe(this.element)
  }

  intersectingHandler (e) {
    if (e[0].isIntersecting) {
      this.playAnimationTimeline()
    }
  }

  playAnimationTimeline () {
    const tl = new TimelineLite({ delay: 0.5 })

    if (this.hasTitleTarget) {
      tl.to(this.titleTarget, { opacity: 1, y: 0, duration: 3, ease: 'expo.out' }, "slide")
    }

    if (this.hasSeparatorTarget) {
      tl.to(this.separatorTarget, { width: "100%", opacity: 1, y: 0, duration: 1, ease: 'expo.out' }, "slide+=0.2")
    }

    if (this.hasDescriptionTarget) {
      tl.to(this.descriptionTarget, { opacity: 1, y: 0, duration: 3, ease: 'expo.out' }, "slide+=0.2")
    }

    if (this.hasButtonTarget) {
      tl.to(this.buttonTarget, { opacity: 1, y: 0, duration: 3, ease: 'expo.out' }, "slide+=0.4")
    }

    if (this.hasImageTarget) {
      tl.to(this.imageTarget, { opacity: 1, y: 0, duration: 3, ease: 'expo.out' }, "slide+=0.3")
    }

    if (this.hasImageBackgroundTarget) {
      tl.to(this.imageBackgroundTarget, { height: 0, duration: 3, ease: 'expo.out' }, "slide+=0.3")
    }
  }
}
