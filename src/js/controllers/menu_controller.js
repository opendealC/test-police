import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = ["burger", "wrapper"]

  initialize () {
    this.body = document.body
    this.html = document.documentElement
  }

  connect () {
    this.isOpen = false
  }

  toggleAnimation () {
    this.element.classList.toggle("active", !this.isOpen)

    if (this.hasBurgerTarget) {
      this.burgerTarget.classList.toggle("active", !this.isOpen)
    }

    this.body.classList.toggle("overflow-hidden-body", !this.isOpen)
    this.html.classList.toggle("overflow-hidden-html", !this.isOpen)
    this.isOpen = !this.isOpen
  }
}
