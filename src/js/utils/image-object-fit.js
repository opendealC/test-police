import objectFitImages from 'object-fit-images'

export default () => {
  objectFitImages('img.img-cover')
  objectFitImages('img.img-contain')
}
